package com.iuranwarga.iwak.model;

import lombok.*;

import javax.persistence.*;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detail_iuran_perbulan")
@ToString
public class DetailIuranPerbulan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_detail_iuran_perbulan")
    private int id;
    @ManyToOne
    @JoinColumn(name = "id_iuran_perbulan", nullable = false)
    private IuranPerbulan iuranPerbulan;
    @ManyToOne
    @JoinColumn(name = "id_jenis_iuran", nullable = false)
    private JenisIuran jenisIuran;
}
