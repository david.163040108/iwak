package com.iuranwarga.iwak.model;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "jenis_iuran")
@ToString
public class JenisIuran {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_jenis_iuran")
    private  int id;
    @Column(name = "nama_iuran")
    private String namaIuran;
    @Column(name = "nominal")
    private int nominal;

}
