package com.iuranwarga.iwak.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "iuran_perbulan")
@ToString
public class IuranPerbulan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_iuran_perbulan")
    private Integer id;
    @Column(name = "tanggal_iuran")
    private LocalDate tanggalIuran;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
