package com.iuranwarga.iwak.controller;

import com.iuranwarga.iwak.model.UserRole;
import com.iuranwarga.iwak.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;
    @GetMapping("/")
    public String hello() {
        String hello = "HelloWorld";
        return hello;
    }

    @GetMapping("/userrole")
    public List<UserRole> list(){
        return userRoleService.roleList();
    }
    @GetMapping("/userrole/{id}")
    public ResponseEntity<UserRole> get(@PathVariable Integer id) {
        try {
            UserRole userRole = userRoleService.get(id);
            return new ResponseEntity<UserRole>(userRole, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<UserRole>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/userrole/{id}")
    public void delete(@PathVariable Integer id) {
        userRoleService.delete(id);
    }
}
