package com.iuranwarga.iwak.controller;

import com.iuranwarga.iwak.model.User;
import com.iuranwarga.iwak.model.UserRole;
import com.iuranwarga.iwak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/user")
    public List<User> list(){
        return userService.userList();
    }
}
