package com.iuranwarga.iwak.service;

import com.iuranwarga.iwak.model.User;
import com.iuranwarga.iwak.model.UserRole;
import com.iuranwarga.iwak.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    public List<UserRole> roleList(){
        return  userRoleRepository.findAll();
    }
    public void save(UserRole userRole) {
        userRoleRepository.save(userRole);
    }

    public UserRole get(Integer id) {
        return userRoleRepository.findById(id).get();
    }

    public void delete(Integer id) {
        userRoleRepository.deleteById(id);
    }
}
