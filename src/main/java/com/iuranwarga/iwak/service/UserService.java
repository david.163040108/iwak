package com.iuranwarga.iwak.service;

import com.iuranwarga.iwak.model.User;
import com.iuranwarga.iwak.model.UserRole;
import com.iuranwarga.iwak.repository.UserRepository;
import com.iuranwarga.iwak.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> userList(){
        return  userRepository.findAll();
    }
    public void save(User user) {
        userRepository.save(user);
    }

    public User get(Integer id) {
        return userRepository.findById(id).get();
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }
}
