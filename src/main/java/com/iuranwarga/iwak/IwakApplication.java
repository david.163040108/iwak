package com.iuranwarga.iwak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IwakApplication {

	public static void main(String[] args) {
		SpringApplication.run(IwakApplication.class, args);
	}

}
