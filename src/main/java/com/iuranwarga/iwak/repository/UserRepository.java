package com.iuranwarga.iwak.repository;

import com.iuranwarga.iwak.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
