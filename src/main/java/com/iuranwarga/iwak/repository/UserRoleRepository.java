package com.iuranwarga.iwak.repository;

import com.iuranwarga.iwak.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository  extends JpaRepository<UserRole, Integer> {
}
